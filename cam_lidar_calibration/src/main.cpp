#include "main.h"

namespace extrinsic_calibration
{
feature_extraction::feature_extraction()
{
}

void feature_extraction::onInit()
{
  // Read input parameters from configuration file
  pkg_loc = ros::package::getPath("cam_lidar_calibration");
  std::ifstream infile(pkg_loc + "/cfg/initial_params.txt");

  // TODO: make some of these rosparams & load from cal file
  infile >> i_params.camera_topic;
  infile >> i_params.lidar_topic;
  infile >> i_params.fisheye_model;
  infile >> i_params.lidar_ring_count;
  infile >> cb_l;
  infile >> cb_b;
  i_params.grid_size = std::make_pair(cb_l, cb_b);
  infile >> i_params.square_length;
  infile >> l;
  infile >> b;
  i_params.board_dimension = std::make_pair(l, b);
  infile >> e_l;
  infile >> e_b;
  i_params.cb_translation_error = std::make_pair(e_l, e_b);
  double camera_mat[9];
  for (int i = 0; i < 9; i++)
  {
    infile >> camera_mat[i];
  }
  cv::Mat(3, 3, CV_64F, &camera_mat).copyTo(i_params.camera_mat);
  infile >> i_params.dist_coeff_num;
  double dist_coeff[i_params.dist_coeff_num];
  for (int i = 0; i < i_params.dist_coeff_num; i++)
  {
    infile >> dist_coeff[i];
  }
  cv::Mat(1, i_params.dist_coeff_num, CV_64F, &dist_coeff).copyTo(i_params.dist_coeff);

  std::cout << "Input parameters received" << std::endl;

  // Creating ROS nodehandle
  ros::NodeHandle& private_nh = getNodeHandle();
  ros::NodeHandle& public_nh = getPrivateNodeHandle();
  ros::NodeHandle& pnh = getMTPrivateNodeHandle();

  it_.reset(new image_transport::ImageTransport(public_nh));
  it_p_.reset(new image_transport::ImageTransport(private_nh));
  image_sub = new image_sub_type(public_nh, i_params.camera_topic, queue_rate);
  pcl_sub = new pc_sub_type(public_nh, i_params.lidar_topic, queue_rate);

  // Dynamic reconfigure gui to set the experimental region bounds
  server = boost::make_shared<dynamic_reconfigure::Server<cam_lidar_calibration::boundsConfig>>(pnh);
  dynamic_reconfigure::Server<cam_lidar_calibration::boundsConfig>::CallbackType f;
  f = boost::bind(&feature_extraction::boundsCallback, this, _1, _2);
  server->setCallback(f);

  // Synchronizer to get synchronized camera-lidar scan pairs
  sync = new message_filters::Synchronizer<MySyncPolicy>(MySyncPolicy(queue_rate), *image_sub, *pcl_sub);
  sync->registerCallback(boost::bind(&feature_extraction::extractROI, this, _1, _2));

  roi_publisher = public_nh.advertise<cam_lidar_calibration::calibration_data>("roi/points", 10, true);
  pub_cloud = public_nh.advertise<sensor_msgs::PointCloud2>("velodyne_features", 1);
  pub_exp_region = public_nh.advertise<sensor_msgs::PointCloud2>("Experimental_region", 10);
  flag_subscriber = public_nh.subscribe<std_msgs::Int8>("/flag", 1, &feature_extraction::flagCallback, this);
  vis_pub = public_nh.advertise<visualization_msgs::Marker>("visualization_marker", 0);
  cb_pub = public_nh.advertise<visualization_msgs::Marker>("board_corners", 0);
  image_publisher = it_p_->advertise("camera_features", 1);
  NODELET_INFO_STREAM("Camera Lidar Calibration");
}

void feature_extraction::flagCallback(const std_msgs::Int8::ConstPtr& msg)
{
  flag = msg->data;  // read flag published by input_sample node

  if (flag == 5)
  {
    add_example_set();
    flag = 0;
  }
}

void feature_extraction::boundsCallback(cam_lidar_calibration::boundsConfig& config, uint32_t level)
{
  // Read the values corresponding to the motion of slider bars in reconfigure gui
  bound = config;
  ROS_INFO("Reconfigure Request: %lf %lf %lf %lf %lf %lf", config.x_min, config.x_max, config.y_min, config.y_max,
           config.z_min, config.z_max);
}

// Convert 3D points w.r.t camera frame to 2D pixel points in image frame
double* feature_extraction::convertToImgPts(double x, double y, double z)
{
  double tmpxC = x / z;
  double tmpyC = y / z;
  cv::Point2d planepointsC;
  planepointsC.x = tmpxC;
  planepointsC.y = tmpyC;
  double r2 = tmpxC * tmpxC + tmpyC * tmpyC;

  if (i_params.fisheye_model)  // distortion function for a fisheye lens
  {
    double r1 = pow(r2, 0.5);
    double a0 = std::atan(r1);
    double a1 =
        a0 * (1 + i_params.dist_coeff.at<double>(0) * pow(a0, 2) + i_params.dist_coeff.at<double>(1) * pow(a0, 4) +
              i_params.dist_coeff.at<double>(2) * pow(a0, 6) + i_params.dist_coeff.at<double>(3) * pow(a0, 8));
    planepointsC.x = (a1 / r1) * tmpxC;
    planepointsC.y = (a1 / r1) * tmpyC;
    planepointsC.x = i_params.camera_mat.at<double>(0, 0) * planepointsC.x + i_params.camera_mat.at<double>(0, 2);
    planepointsC.y = i_params.camera_mat.at<double>(1, 1) * planepointsC.y + i_params.camera_mat.at<double>(1, 2);
  }
  else  // For pinhole camera model
  {
    double tmpdist = 1 + i_params.dist_coeff.at<double>(0) * r2 + i_params.dist_coeff.at<double>(1) * r2 * r2 +
                     i_params.dist_coeff.at<double>(4) * r2 * r2 * r2;
    planepointsC.x = tmpxC * tmpdist + 2 * i_params.dist_coeff.at<double>(2) * tmpxC * tmpyC +
                     i_params.dist_coeff.at<double>(3) * (r2 + 2 * tmpxC * tmpxC);
    planepointsC.y = tmpyC * tmpdist + i_params.dist_coeff.at<double>(2) * (r2 + 2 * tmpyC * tmpyC) +
                     2 * i_params.dist_coeff.at<double>(3) * tmpxC * tmpyC;
    planepointsC.x = i_params.camera_mat.at<double>(0, 0) * planepointsC.x + i_params.camera_mat.at<double>(0, 2);
    planepointsC.y = i_params.camera_mat.at<double>(1, 1) * planepointsC.y + i_params.camera_mat.at<double>(1, 2);
  }

  double* img_coord = new double[2];
  *(img_coord) = planepointsC.x;
  *(img_coord + 1) = planepointsC.y;

  return img_coord;
}

void feature_extraction::cloudPassthrough(const pcl::PointCloud<pcl::PointXYZIR>::Ptr& cloud_in,
                                          pcl::PointCloud<pcl::PointXYZIR>::Ptr& cloud_passthrough,
                                          const std::string field_name, const float limit_min, const float limit_max)
{
  pcl::PassThrough<pcl::PointXYZIR> pass;
  pass.setInputCloud(cloud_in);
  pass.setFilterFieldName(field_name);
  pass.setFilterLimits(limit_min, limit_max);
  pass.filter(*cloud_passthrough);
}

void feature_extraction::cloudPassthrough(pcl::PointCloud<pcl::PointXYZIR>::Ptr& cloud_passthrough,
                                          const std::string field_name, const float limit_min, const float limit_max)
{
  pcl::PassThrough<pcl::PointXYZIR> pass;
  pass.setInputCloud(cloud_passthrough);
  pass.setFilterFieldName(field_name);
  pass.setFilterLimits(limit_min, limit_max);
  pass.filter(*cloud_passthrough);
}

void feature_extraction::projectInliersPlane(const pcl::PointCloud<pcl::PointXYZIR>::Ptr& cloud_in,
                                             pcl::PointCloud<pcl::PointXYZIR>::Ptr& cloud_projected,
                                             pcl::ModelCoefficients::Ptr& coefficients)
{
  pcl::ProjectInliers<pcl::PointXYZIR> proj;
  proj.setModelType(pcl::SACMODEL_PLANE);
  proj.setInputCloud(cloud_in);
  proj.setModelCoefficients(coefficients);
  proj.filter(*cloud_projected);
}

bool feature_extraction::extractPlaneSegmentation(const pcl::PointCloud<pcl::PointXYZIR>::Ptr& points_in,
                                                  pcl::ModelCoefficients::Ptr& coefficients,
                                                  pcl::PointIndices::Ptr& inliers)
{
  // TODO figure out why segment does not work with PointXYZIR
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_xyz(new pcl::PointCloud<pcl::PointXYZ>);
  cloud_xyz->header.frame_id = points_in->header.frame_id;
  cloud_xyz->header.stamp = points_in->header.stamp;
  for (size_t i = 1; i < points_in->points.size(); ++i)
  {
    pcl::PointXYZ point;
    point.x = points_in->points[i].x;
    point.y = points_in->points[i].y;
    point.z = points_in->points[i].z;
    cloud_xyz->push_back(point);
  }

  pcl::SACSegmentation<pcl::PointXYZ> seg_plane;
  seg_plane.setOptimizeCoefficients(true);
  seg_plane.setModelType(pcl::SACMODEL_PLANE);
  seg_plane.setMethodType(pcl::SAC_RANSAC);  // Random sample consensus
  seg_plane.setMaxIterations(1000);
  seg_plane.setDistanceThreshold(0.004);  // TODO: make this configurable OR find average distance between points
  seg_plane.setInputCloud(cloud_xyz);
  // Plane model segmentation
  seg_plane.segment(*inliers,        // the resultant point indices that support the model found (inliers)
                    *coefficients);  // the resultant model coefficients

  if (inliers->indices.size() == 0)
  {
    ROS_WARN("Could not estimate a planar model for the given dataset.");
    return false;
  }

  return true;
}

void feature_extraction::findEdgePoints(const pcl::PointCloud<pcl::PointXYZIR>::Ptr& points_in,
                                        pcl::PointCloud<pcl::PointXYZIR>::Ptr& right_edge_points,
                                        pcl::PointCloud<pcl::PointXYZIR>::Ptr& left_edge_points)
{
  // First: Sort out the points in the point cloud according to their ring numbers
  std::vector<std::deque<pcl::PointXYZIR*>> candidate_segments(i_params.lidar_ring_count);

  for (size_t i = 0; i < points_in->points.size(); ++i)
  {
    int ring_number = static_cast<int>(points_in->points[i].ring);

    // push back the points in a particular ring number
    candidate_segments[ring_number].push_back(&(points_in->points[i]));
  }

  // Second: Arrange points in every ring in descending order of y coordinate (left to right)
  for (size_t i = 0; i < candidate_segments.size(); i++)
  {
    // If no points belong to a particular ring number
    if (candidate_segments[i].size() == 0)
    {
      continue;
    }
    for (size_t j = 0; j < candidate_segments[i].size(); j++)
    {
      for (size_t k = j + 1; k < candidate_segments[i].size(); k++)
      {
        // If there is a larger element found on right of the point, swap
        if (candidate_segments[i][j]->y < candidate_segments[i][k]->y)
        {
          pcl::PointXYZIR temp;
          temp = *candidate_segments[i][k];
          *candidate_segments[i][k] = *candidate_segments[i][j];
          *candidate_segments[i][j] = temp;
        }
      }
    }
  }

  pcl::PointXYZIR rightmost_point, leftmost_point;
  // Third: Find minimum and maximum points in a ring
  for (size_t i = 0; i < candidate_segments.size(); i++)
  {
    // If no points belong to a particular ring number
    if (candidate_segments[i].size() == 0)
    {
      continue;
    }
    leftmost_point = *candidate_segments[i][0];
    rightmost_point = *candidate_segments[i][candidate_segments[i].size() - 1];
    left_edge_points->push_back(leftmost_point);
    right_edge_points->push_back(rightmost_point);
  }
}

bool feature_extraction::extractLinesSegmentation(const pcl::PointCloud<pcl::PointXYZIR>::Ptr& edge_points,
                                                  pcl::ModelCoefficients::Ptr& coefficients_1,
                                                  pcl::PointIndices::Ptr& inliers_1,
                                                  pcl::ModelCoefficients::Ptr& coefficients_2,
                                                  pcl::PointIndices::Ptr& inliers_2)
{
  // Fitting line_1 through edge_points  (top or bottom line)
  pcl::SACSegmentation<pcl::PointXYZIR> seg_line;
  seg_line.setModelType(pcl::SACMODEL_LINE);
  seg_line.setMethodType(pcl::SAC_RANSAC);
  seg_line.setDistanceThreshold(0.02);
  seg_line.setInputCloud(edge_points);
  seg_line.segment(*inliers_1, *coefficients_1);
  if (inliers_1->indices.size() == 0)
  {
    ROS_WARN("[extractLinesSegmentation inliers_1] Could not estimate a planar model for the given dataset.");
    return false;
  }

  // Filter out line_1
  pcl::PointCloud<pcl::PointXYZIR>::Ptr cloud_extracted(new pcl::PointCloud<pcl::PointXYZIR>);
  pcl::ExtractIndices<pcl::PointXYZIR> extract;
  extract.setInputCloud(edge_points);
  extract.setIndices(inliers_1);
  extract.setNegative(true);
  extract.filter(*cloud_extracted);

  // Fitting line_2 through edge_points
  seg_line.setInputCloud(cloud_extracted);
  seg_line.segment(*inliers_2, *coefficients_2);
  if (inliers_2->indices.size() == 0)
  {
    ROS_WARN("[extractLinesSegmentation inliers_2]  Could not estimate a planar model for the given dataset.");
    return false;
  }

  return true;
}

bool feature_extraction::extractLinesHybrid(const pcl::PointCloud<pcl::PointXYZIR>::Ptr& edge_points,
                                            pcl::ModelCoefficients::Ptr& coefficients_1,
                                            pcl::PointIndices::Ptr& inliers_1,
                                            pcl::ModelCoefficients::Ptr& coefficients_2,
                                            pcl::PointIndices::Ptr& inliers_2, const bool is_left)
{
  ROS_INFO("extractLinesHybrid");
  // Fitting line_1 through edge_points (top or bottom line)
  pcl::SACSegmentation<pcl::PointXYZIR> seg_line;
  seg_line.setModelType(pcl::SACMODEL_LINE);
  seg_line.setMethodType(pcl::SAC_RANSAC);
  seg_line.setDistanceThreshold(0.01);
  seg_line.setInputCloud(edge_points);
  seg_line.segment(*inliers_1, *coefficients_1);
  if (inliers_1->indices.size() == 0)
  {
    ROS_WARN("[extractLinesHybrid inliers_1] Could not estimate a planar model for the given dataset.");
    return false;
  }

  pcl::PointCloud<pcl::PointXYZIR>::Ptr edge_points_1(new pcl::PointCloud<pcl::PointXYZIR>);
  pcl::PointCloud<pcl::PointXYZIR>::Ptr edge_points_2(new pcl::PointCloud<pcl::PointXYZIR>);
  edge_points_1->header.frame_id = edge_points->header.frame_id;
  edge_points_1->header.stamp = edge_points->header.stamp;
  edge_points_2->header.frame_id = edge_points->header.frame_id;
  edge_points_2->header.stamp = edge_points->header.stamp;

  // Split edge_points into edge_points_1 and edge_points_2
  double board_ring_avg =
      static_cast<double>(edge_points->points[edge_points->points.size() - 1].ring + edge_points->points[0].ring) / 2.0;
  int corner_idx = 0;
  bool even_split = false;
  for (size_t i = 0; i < edge_points->points.size(); ++i)
  {
    ROS_INFO("i = %zu | x = %f | y = %f | z = %f | ring = %d", i, edge_points->points[i].x, edge_points->points[i].y,
             edge_points->points[i].z, edge_points->points[i].ring);
    if (i == 0)
      continue;
    // y + and - is flipped
    // if left edge points, find leftmost point (-y)
    if (is_left)
    {
      if (edge_points->points[corner_idx].y <= edge_points->points[i].y)
      {
        corner_idx = i;
        if (edge_points->points[i].y == edge_points->points[i - 1].y)
        {
          ROS_INFO("even_split = true");
          even_split = true;
        }
      }
    }
    // if right edge points, find rightmost point (+y)
    else
    {
      if (edge_points->points[corner_idx].y >= edge_points->points[i].y)
      {
        corner_idx = i;
        if (edge_points->points[i].y == edge_points->points[i - 1].y)
        {
          ROS_INFO("even_split = true");
          even_split = true;
        }
      }
    }
  }
  int ring_corner = edge_points->points[corner_idx].ring;
  double ring_avg = (edge_points->points[inliers_1->indices[inliers_1->indices.size() - 1]].ring +
                     edge_points->points[inliers_1->indices[0]].ring) /
                    2.0;
  ROS_INFO("[corner] ring = %d", ring_corner);
  ROS_INFO("first line ring (avg) = %f | board ring (avg) = %f", ring_avg, board_ring_avg);
  // top line found first (edge_points_1 = top left line)
  if (ring_avg > board_ring_avg)
  {
    ROS_INFO("top line found first");
    for (size_t i = 0; i < edge_points->points.size(); ++i)
    {
      /* split edge_points into edge_points_1 & edge_points_2
         (even_split = false)
                                               *
                   left_edge_points_1 -----> *---* <----- right_edge_points_1
                left_edge_points_1 ----->  *-------* <----- right_edge_points_1
              left_edge_points_1 ----->  *-----------* <----- right_edge_points_1
      left_edge_points_1 & _2 -----> ==*-----------*== <----- right_edge_points_1 & _2
              left_edge_points_2 ----->  *-------* <----- right_edge_points_2
                                           *   *
                                             *
      */
      // edge_points_1 contains all points above corner point
      if (edge_points->points[i].ring > ring_corner)
      {
        edge_points_1->push_back(edge_points->points[i]);
      }
      // edge_points_2 contains all points below corner point
      else if (edge_points->points[i].ring < ring_corner)
      {
        edge_points_2->push_back(edge_points->points[i]);
      }
      // corner point is shared in both edge_points_1 & edge_points_2
      else if (!even_split)
      {
        edge_points_1->push_back(edge_points->points[i]);
        edge_points_2->push_back(edge_points->points[i]);
      }
      /* if two equal corner points found, do not share between edge_points_1 & edge_points_2
         (even_split = true)
                *
              *---*
            *       *
          *-----------*
      ==*           *== corner
          *-------*
            *   *
              *
      */
      else
      {
        edge_points_1->push_back(edge_points->points[i]);
      }
    }
  }
  // bottom line found first (edge_points_1 = bottom left line)
  else if (ring_avg < board_ring_avg)
  {
    ROS_INFO("bottom line found first");
    for (size_t i = 1; i < edge_points->points.size(); ++i)
    {
      /* split edge_points into edge_points_1 & edge_points_2
         (even_split = false)
                                               *
                   left_edge_points_2 -----> *---* <----- right_edge_points_2
                left_edge_points_2 ----->  *-------* <----- right_edge_points_2
              left_edge_points_2 ----->  *-----------* <----- right_edge_points_2
      left_edge_points_1 & _2 -----> ==*-----------*== <----- right_edge_points_1 & _2
              left_edge_points_1 ----->  *-------* <----- right_edge_points_1
                                           *   *
                                             *
      */
      // edge_points_2 contains all points above corner point
      if (edge_points->points[i].ring > ring_corner)
      {
        edge_points_2->push_back(edge_points->points[i]);
      }
      // edge_points_1 contains all points below corner point
      else if (edge_points->points[i].ring < ring_corner)
      {
        edge_points_1->push_back(edge_points->points[i]);
      }
      // corner point is shared in both edge_points_1 & edge_points_2
      else if (!even_split)
      {
        edge_points_1->push_back(edge_points->points[i]);
        edge_points_2->push_back(edge_points->points[i]);
      }
      /* if two equal corner points found, do not share between edge_points_1 & edge_points_2
         (even_split = true)
                *
              *---*
            *       *
          *-----------*
      ==*           *== corner
          *-------*
            *   *
              *
      */
      else
      {
        edge_points_2->push_back(edge_points->points[i]);
      }
    }
  }
  else
  {
    ROS_ERROR("SACSegmentation failed to split top and bottom lines. Try again");
    return false;
  }

  ROS_INFO("edge_points: %zu", edge_points->size());
  ROS_INFO("edge_points_1: %zu", edge_points_1->size());
  ROS_INFO("edge_points_2: %zu", edge_points_2->size());

  if (edge_points_1->size() < 2 || edge_points_2->size() < 2)
  {
    ROS_ERROR("edge_points_1 or edge_points_2 does not have enough points");
    return false;
  }
  // need at least 3 points for SACSegmentation
  else if (edge_points_1->size() == 2)
  {
    edge_points_1->push_back(edge_points_1->points[0]);
  }
  // need at least 3 points for SACSegmentation
  else if (edge_points_2->size() == 2)
  {
    edge_points_2->push_back(edge_points_2->points[0]);
  }

  // Fitting left_line_2 through edge_points
  pcl::SACSegmentation<pcl::PointXYZIR> seg_line_xyz;
  seg_line_xyz.setModelType(pcl::SACMODEL_LINE);
  seg_line_xyz.setMethodType(pcl::SAC_RANSAC);
  seg_line_xyz.setDistanceThreshold(1.0);
  seg_line_xyz.setInputCloud(edge_points_2);
  seg_line_xyz.segment(*inliers_2, *coefficients_2);
  if (inliers_2->indices.size() == 0)
  {
    ROS_WARN("[extractLinesHybrid inliers_2] Could not estimate a planar model for the given dataset.");
    return false;
  }

  return true;
}

// Extract features of interest
void feature_extraction::extractROI(const sensor_msgs::Image::ConstPtr& img_msg_in,
                                    const sensor_msgs::PointCloud2::ConstPtr& pcd_msg_in)
{
  pcl::PointCloud<pcl::PointXYZIR>::Ptr cloud_in(new pcl::PointCloud<pcl::PointXYZIR>);
  pcl::PointCloud<pcl::PointXYZIR>::Ptr cloud_passthrough(new pcl::PointCloud<pcl::PointXYZIR>);
  pcl::fromROSMsg(*pcd_msg_in, *cloud_in);

  // Filter out the experimental region
  cloudPassthrough(cloud_in, cloud_passthrough, "x", bound.x_min, bound.x_max);
  cloudPassthrough(cloud_passthrough, "z", bound.z_min, bound.z_max);
  cloudPassthrough(cloud_passthrough, "y", bound.y_min, bound.y_max);

  // Publish the experimental region point cloud
  sensor_msgs::PointCloud2 pcd_exp_region;
  pcl::toROSMsg(*cloud_passthrough, pcd_exp_region);
  pub_exp_region.publish(pcd_exp_region);

  // If user presses 'i' to get a sample
  if (flag == 1)
  {
    flag = 0;  // reset flag
    /******************************************** IMAGE FEATURES ********************************************/
    cv::Mat corner_vectors = cv::Mat::eye(3, 5, CV_64F);  // identity matrix of the specified size and type
    cv::Mat chessboard_normal = cv::Mat(1, 3, CV_64F);
    std::vector<cv::Point2f> image_points;           // checkerboard corner points
    std::vector<cv::Point2f> img_square_corner_pts;  // middle square corner points
    std::vector<cv::Point2f> img_board_corner_pts;   // board corner and centre points

    cv_bridge::CvImagePtr cv_ptr;
    cv::Size2i patternNum(i_params.grid_size.first, i_params.grid_size.second);
    cv::Size2i patternSize(i_params.square_length, i_params.square_length);

    try
    {
      cv_ptr = cv_bridge::toCvCopy(img_msg_in, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
    }

    cv::Mat gray;
    std::vector<cv::Point2f> corners;
    std::vector<cv::Point3f> grid3dPoint;
    cv::cvtColor(cv_ptr->image, gray, CV_BGR2GRAY);
    // Find checkerboard pattern in the image
    bool patternfound =
        cv::findChessboardCorners(gray, patternNum, corners, CALIB_CB_ADAPTIVE_THRESH + CALIB_CB_NORMALIZE_IMAGE);

    if (patternfound)
    {
      // Find corner points with sub-pixel accuracy
      cornerSubPix(gray,          // Input single-channel, 8-bit or float image.
                   corners,       // Initial coordinates of the input corners & refined coordinates provided for output
                   Size(11, 11),  // winSize: (11 x 2 + 1) x (11 x 2 + 1) = 23 x 23 search window
                   Size(-1, -1),  // zeroZone: (-1, -1) = no such size
                   TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));  // criteria
      // cv::Size img_size;
      // img_size.height = cv_ptr->image.rows;
      // img_size.width = cv_ptr->image.cols;
      double tx, ty;  // Translation values
      // Location of board frame origin from the bottom left inner corner of the checkerboard
      tx = (patternNum.height - 1) * patternSize.height / 2;
      ty = (patternNum.width - 1) * patternSize.width / 2;
      // checkerboard's square corners w.r.t board frame
      for (int i = 0; i < patternNum.height; i++)
      {
        for (int j = 0; j < patternNum.width; j++)
        {
          cv::Point3f point;
          // Translating origin from bottom left corner to the centre of the checkerboard
          point.x = i * patternSize.height - tx;
          point.y = j * patternSize.width - ty;
          point.z = 0;
          grid3dPoint.push_back(point);
        }
      }
      std::vector<cv::Point3f> board_corners;
      // Board corner coordinates from the centre of the checkerboard
      board_corners.push_back(cv::Point3f((i_params.board_dimension.second - i_params.cb_translation_error.second) / 2,
                                          (i_params.board_dimension.first - i_params.cb_translation_error.first) / 2,
                                          0.0));
      board_corners.push_back(cv::Point3f(-(i_params.board_dimension.second + i_params.cb_translation_error.second) / 2,
                                          (i_params.board_dimension.first - i_params.cb_translation_error.first) / 2,
                                          0.0));
      board_corners.push_back(cv::Point3f(-(i_params.board_dimension.second + i_params.cb_translation_error.second) / 2,
                                          -(i_params.board_dimension.first + i_params.cb_translation_error.first) / 2,
                                          0.0));
      board_corners.push_back(cv::Point3f((i_params.board_dimension.second - i_params.cb_translation_error.second) / 2,
                                          -(i_params.board_dimension.first + i_params.cb_translation_error.first) / 2,
                                          0.0));
      // Board centre coordinates from the centre of the checkerboard (due to incorrect placement of checkerbord on
      // board)
      board_corners.push_back(
          cv::Point3f(-i_params.cb_translation_error.second / 2, -i_params.cb_translation_error.first / 2, 0.0));

      std::vector<cv::Point3f> square_edge;
      // centre checkerboard square corner coordinates w.r.t. the centre of the checkerboard (origin)
      square_edge.push_back(cv::Point3f(-i_params.square_length / 2, -i_params.square_length / 2, 0.0));
      square_edge.push_back(cv::Point3f(i_params.square_length / 2, i_params.square_length / 2, 0.0));
      // Initialization for pinhole and fisheye cameras
      cv::Mat rvec(3, 3, cv::DataType<double>::type);  // rotation
      cv::Mat tvec(3, 1, cv::DataType<double>::type);  // translation

      if (i_params.fisheye_model)
      {
        std::vector<cv::Point2f> corners_undistorted;
        // Undistort the image by applying the fisheye intrinsic parameters
        cv::fisheye::undistortPoints(corners,               // distorted
                                     corners_undistorted,   // undistorted
                                     i_params.camera_mat,   // camera intrinsic matrix
                                     i_params.dist_coeff,   // input vector of distortion coefficients
                                     i_params.camera_mat);  // camera matrix in the new or rectified coordinate frame
                                                            // We set this to be the same as i_params.camera_mat
                                                            // or else it will be set to empty matrix by default.
        cv::Mat empty_dist_coeff = (Mat_<double>(4, 1) << 0, 0, 0, 0);
        cv::solvePnP(grid3dPoint,          // objectPoints
                     corners_undistorted,  // imagePoints
                     i_params.camera_mat,  // input camera intrinsic matrix
                     empty_dist_coeff,     // input vector of distortion coefficients
                     rvec,                 // output rotation vector
                     tvec);                // output translation vector
        // Projects points using fisheye model
        cv::fisheye::projectPoints(grid3dPoint,           // objectPoints
                                   image_points,          // imagePoints
                                   rvec,                  // rotation vector
                                   tvec,                  // translation vector
                                   i_params.camera_mat,   // camera intrinsic matrix
                                   i_params.dist_coeff);  // vector of distortion coefficients
        // Mark the centre square corner points
        cv::fisheye::projectPoints(square_edge, img_square_corner_pts, rvec, tvec, i_params.camera_mat,
                                   i_params.dist_coeff);
        cv::fisheye::projectPoints(board_corners, img_board_corner_pts, rvec, tvec, i_params.camera_mat,
                                   i_params.dist_coeff);
        // Visualize
        for (size_t i = 0; i < grid3dPoint.size(); i++)
          cv::circle(cv_ptr->image, image_points[i], 5, CV_RGB(255, 0, 0), -1);
        for (size_t i = 0; i < square_edge.size(); i++)
          cv::circle(cv_ptr->image, img_square_corner_pts[i], 5, CV_RGB(255, 0, 0), -1);
        for (size_t i = 0; i < board_corners.size(); i++)
          cv::circle(cv_ptr->image, img_board_corner_pts[i], 5, CV_RGB(255, 0, 0), -1);
      }
      // Pinhole model
      else
      {
        cv::solvePnP(grid3dPoint, corners, i_params.camera_mat, i_params.dist_coeff, rvec, tvec);
        cv::projectPoints(grid3dPoint, rvec, tvec, i_params.camera_mat, i_params.dist_coeff, image_points);
        // Mark the centre square corner points
        cv::projectPoints(square_edge, rvec, tvec, i_params.camera_mat, i_params.dist_coeff, img_square_corner_pts);
        cv::projectPoints(board_corners, rvec, tvec, i_params.camera_mat, i_params.dist_coeff, img_board_corner_pts);
        // Visualize
        for (size_t i = 0; i < grid3dPoint.size(); i++)
          cv::circle(cv_ptr->image, image_points[i], 5, CV_RGB(255, 0, 0), -1);
        for (size_t i = 0; i < square_edge.size(); i++)
          cv::circle(cv_ptr->image, img_square_corner_pts[i], 5, CV_RGB(255, 0, 0), -1);
        for (size_t i = 0; i < board_corners.size(); i++)
          cv::circle(cv_ptr->image, img_board_corner_pts[i], 5, CV_RGB(255, 0, 0), -1);
      }

      // chessboard_pose is a 3x4 transform matrix that transforms points in board frame to camera frame | R&T
      cv::Mat chessboard_pose = cv::Mat::eye(4, 4, CV_64F);
      cv::Mat rmat = cv::Mat(3, 3, CV_64F);  // rotation matrix
      cv::Rodrigues(rvec, rmat);             // Euler angles to rotation matrix

      for (int j = 0; j < 3; j++)
      {
        for (int k = 0; k < 3; k++)
        {
          chessboard_pose.at<double>(j, k) = rmat.at<double>(j, k);
        }
        chessboard_pose.at<double>(j, 3) = tvec.at<double>(j);
      }

      chessboard_normal.at<double>(0) = 0;
      chessboard_normal.at<double>(1) = 0;
      chessboard_normal.at<double>(2) = 1;
      chessboard_normal = chessboard_normal * chessboard_pose(cv::Rect(0, 0, 3, 3)).t();

      for (size_t k = 0; k < board_corners.size(); k++)
      {
        // take every point in board_corners set
        cv::Point3f pt(board_corners[k]);
        for (int i = 0; i < 3; i++)
        {
          // Transform it to obtain the coordinates in cam frame
          corner_vectors.at<double>(i, k) = chessboard_pose.at<double>(i, 0) * pt.x +
                                            chessboard_pose.at<double>(i, 1) * pt.y + chessboard_pose.at<double>(i, 3);
        }

        // convert 3D coordinates to image coordinates
        double* img_coord = feature_extraction::convertToImgPts(
            corner_vectors.at<double>(0, k), corner_vectors.at<double>(1, k), corner_vectors.at<double>(2, k));
        // Mark the corners and the board centre
        if (k == 0)
          cv::circle(cv_ptr->image, cv::Point(img_coord[0], img_coord[1]), 8, CV_RGB(0, 255, 0), -1);  // green
        else if (k == 1)
          cv::circle(cv_ptr->image, cv::Point(img_coord[0], img_coord[1]), 8, CV_RGB(255, 255, 0), -1);  // yellow
        else if (k == 2)
          cv::circle(cv_ptr->image, cv::Point(img_coord[0], img_coord[1]), 8, CV_RGB(0, 0, 255), -1);  // blue
        else if (k == 3)
          cv::circle(cv_ptr->image, cv::Point(img_coord[0], img_coord[1]), 8, CV_RGB(255, 0, 0), -1);  // red
        else
          cv::circle(cv_ptr->image, cv::Point(img_coord[0], img_coord[1]), 8, CV_RGB(255, 255, 255), -1);  // white

        delete[] img_coord;
      }
      // Publish the image with all the features marked in it
      image_publisher.publish(cv_ptr->toImageMsg());
    }  // if (patternfound)

    else
    {
      ROS_ERROR("[findChessboardCorners] Pattern not found on the image!");
      return;
    }

    /******************************************** POINT CLOUD FEATURES ********************************************/

    // Filter out everything below the board from the ROI pointcloud (such as legs)

    // find the point with max height(z val) in cloud_passthrough
    double z_max = cloud_passthrough->points[0].z;
    for (size_t i = 1; i < cloud_passthrough->points.size(); ++i)
    {
      if (cloud_passthrough->points[i].z > z_max)
      {
        z_max = cloud_passthrough->points[i].z;
      }
    }
    // subtract by approximate diagonal length (in metres)
    double z_min =
        z_max - sqrt(pow(i_params.board_dimension.first, 2) + pow(i_params.board_dimension.second, 2)) / 1000;
    pcl::PointCloud<pcl::PointXYZIR>::Ptr points_board(new pcl::PointCloud<pcl::PointXYZIR>);
    points_board->header.frame_id = cloud_in->header.frame_id;
    points_board->header.stamp = cloud_in->header.stamp;
    cloudPassthrough(cloud_passthrough, points_board, "z", z_min, z_max);

    // Fit a plane through the board point cloud
    pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients());
    pcl::PointIndices::Ptr inliers(new pcl::PointIndices());
    extractPlaneSegmentation(points_board, coefficients, inliers);

    // Plane normal vector magnitude
    float mag =
        sqrt(pow(coefficients->values[0], 2) + pow(coefficients->values[1], 2) + pow(coefficients->values[2], 2));

    // Project the inliers on the fit plane
    pcl::PointCloud<pcl::PointXYZIR>::Ptr cloud_projected(new pcl::PointCloud<pcl::PointXYZIR>);
    projectInliersPlane(points_board, cloud_projected, coefficients);

    // Publish the projected inliers
    sensor_msgs::PointCloud2 pcd_msg_out;
    pcl::toROSMsg(*points_board, pcd_msg_out);
    pub_cloud.publish(pcd_msg_out);

    // Find the max and min points in every ring corresponding to the board
    pcl::PointCloud<pcl::PointXYZIR>::Ptr right_edge_points(new pcl::PointCloud<pcl::PointXYZIR>);
    right_edge_points->header.frame_id = cloud_in->header.frame_id;
    right_edge_points->header.stamp = cloud_in->header.stamp;
    pcl::PointCloud<pcl::PointXYZIR>::Ptr left_edge_points(new pcl::PointCloud<pcl::PointXYZIR>);
    left_edge_points->header.frame_id = cloud_in->header.frame_id;
    left_edge_points->header.stamp = cloud_in->header.stamp;
    findEdgePoints(cloud_projected, right_edge_points, left_edge_points);

    // Fit lines through minimum and maximum points
    pcl::ModelCoefficients::Ptr coefficients_left_1(new pcl::ModelCoefficients);
    pcl::PointIndices::Ptr inliers_left_1(new pcl::PointIndices);
    pcl::ModelCoefficients::Ptr coefficients_left_2(new pcl::ModelCoefficients);
    pcl::PointIndices::Ptr inliers_left_2(new pcl::PointIndices);
    pcl::ModelCoefficients::Ptr coefficients_right_1(new pcl::ModelCoefficients);
    pcl::PointIndices::Ptr inliers_right_1(new pcl::PointIndices);
    pcl::ModelCoefficients::Ptr coefficients_right_2(new pcl::ModelCoefficients);
    pcl::PointIndices::Ptr inliers_right_2(new pcl::PointIndices);

    // left edge
    if (!extractLinesSegmentation(left_edge_points, coefficients_left_1, inliers_left_1, coefficients_left_2,
                                  inliers_left_2))
    {
      // if failed to extract line segmentation, try hybrid
      if (!extractLinesHybrid(right_edge_points, coefficients_left_1, inliers_left_1, coefficients_left_2,
                              inliers_left_2, true))
      {
        ROS_ERROR("Segmentation extraction not successful. Try sampling again [i]");
        return;
      }
    }
    // right edge
    if (!extractLinesSegmentation(right_edge_points, coefficients_right_1, inliers_right_1, coefficients_right_2,
                                  inliers_right_2))
    {
      // if failed to extract line segmentation, try hybrid
      if (!extractLinesHybrid(right_edge_points, coefficients_right_1, inliers_right_1, coefficients_right_2,
                              inliers_right_2, false))
      {
        ROS_ERROR("Segmentation extraction not successful. Try sampling again [i]");
        return;
      }
    }
    ROS_INFO("Segmentation successful. Save the sample with [Enter]");

    // Find out two (out of the four) intersection points
    Eigen::Vector4f intersecting_point;
    pcl::PointCloud<pcl::PointXYZ>::Ptr points_intersect(new pcl::PointCloud<pcl::PointXYZ>);
    if (pcl::lineWithLineIntersection(*coefficients_left_1, *coefficients_left_2, intersecting_point))
    {
      // BLUE
      pcl::PointXYZ point;
      point.x = intersecting_point[0];
      point.y = intersecting_point[1];
      point.z = intersecting_point[2];
      points_intersect->points.push_back(point);
    }
    if (pcl::lineWithLineIntersection(*coefficients_right_1, *coefficients_right_2, intersecting_point))
    {
      // GREEN
      pcl::PointXYZ point;
      point.x = intersecting_point[0];
      point.y = intersecting_point[1];
      point.z = intersecting_point[2];
      points_intersect->points.push_back(point);
    }

    // To determine which two lines intersect, find which lines are at top and which lines are at bottom
    bool right_1_is_top = (right_edge_points->points[inliers_right_1->indices[0]].ring >
                           right_edge_points->points[inliers_right_2->indices[0]].ring);
    bool left_1_is_top = (left_edge_points->points[inliers_left_1->indices[0]].ring >
                          left_edge_points->points[inliers_left_2->indices[0]].ring);
    if (right_1_is_top ^ left_1_is_top)
    {
      // Find out the line intersection between particular lines in left_edge_points and right_edge_points
      if (pcl::lineWithLineIntersection(*coefficients_left_2, *coefficients_right_1, intersecting_point))
      {
        // RED
        pcl::PointXYZ point;
        point.x = intersecting_point[0];
        point.y = intersecting_point[1];
        point.z = intersecting_point[2];
        points_intersect->points.push_back(point);
      }
      if (pcl::lineWithLineIntersection(*coefficients_left_1, *coefficients_right_2, intersecting_point))
      {
        // YELLOW
        pcl::PointXYZ point;
        point.x = intersecting_point[0];
        point.y = intersecting_point[1];
        point.z = intersecting_point[2];
        points_intersect->points.push_back(point);
      }
    }
    else
    {
      // Find out the line intersection between other lines in left_edge_points and right_edge_points
      if (pcl::lineWithLineIntersection(*coefficients_left_2, *coefficients_right_2, intersecting_point))
      {
        // RED
        pcl::PointXYZ point;
        point.x = intersecting_point[0];
        point.y = intersecting_point[1];
        point.z = intersecting_point[2];
        points_intersect->points.push_back(point);
      }
      if (pcl::lineWithLineIntersection(*coefficients_left_1, *coefficients_right_1, intersecting_point))
      {
        // YELLOW
        pcl::PointXYZ point;
        point.x = intersecting_point[0];
        point.y = intersecting_point[1];
        point.z = intersecting_point[2];
        points_intersect->points.push_back(point);
      }
    }

    // input data
    sample_data.lidar_point[0] = (points_intersect->points[0].x + points_intersect->points[1].x) / 2;
    sample_data.lidar_point[1] = (points_intersect->points[0].y + points_intersect->points[1].y) / 2;
    sample_data.lidar_point[2] = (points_intersect->points[0].z + points_intersect->points[1].z) / 2;
    sample_data.lidar_normal[0] = -coefficients->values[0] / mag;
    sample_data.lidar_normal[1] = -coefficients->values[1] / mag;
    sample_data.lidar_normal[2] = -coefficients->values[2] / mag;
    double top_down_radius = sqrt(pow(sample_data.lidar_point[0], 2) + pow(sample_data.lidar_point[1], 2));
    double x_comp = sample_data.lidar_point[0] + sample_data.lidar_normal[0] / 2;
    double y_comp = sample_data.lidar_point[1] + sample_data.lidar_normal[1] / 2;
    double vector_dist = sqrt(pow(x_comp, 2) + pow(y_comp, 2));
    if (vector_dist > top_down_radius)
    {
      sample_data.lidar_normal[0] = -sample_data.lidar_normal[0];
      sample_data.lidar_normal[1] = -sample_data.lidar_normal[1];
      sample_data.lidar_normal[2] = -sample_data.lidar_normal[2];
    }
    sample_data.camera_point[0] = corner_vectors.at<double>(0, 4) / 1000;
    sample_data.camera_point[1] = corner_vectors.at<double>(1, 4) / 1000;
    sample_data.camera_point[2] = corner_vectors.at<double>(2, 4) / 1000;
    sample_data.camera_normal[0] = chessboard_normal.at<double>(0);
    sample_data.camera_normal[1] = chessboard_normal.at<double>(1);
    sample_data.camera_normal[2] = chessboard_normal.at<double>(2);
    sample_data.lidar_corner[0] = points_intersect->points[2].x;
    sample_data.lidar_corner[1] = points_intersect->points[2].y;
    sample_data.lidar_corner[2] = points_intersect->points[2].z;
    sample_data.pixel_data = sqrt(pow((img_square_corner_pts[1].x - img_square_corner_pts[0].x), 2) +
                                  pow((img_square_corner_pts[1].y - img_square_corner_pts[0].y), 2)) /
                             1000;

    // Visualize 4 corner points of velodyne board, the board edge lines and the centre point
    visualization_msgs::Marker board_corner_points_marker;
    board_corner_points_marker.header.frame_id = pcd_msg_in->header.frame_id;
    board_corner_points_marker.header.stamp = ros::Time();
    board_corner_points_marker.ns = "my_sphere";
    board_corner_points_marker.id = 11;
    board_corner_points_marker.type = visualization_msgs::Marker::POINTS;
    board_corner_points_marker.action = visualization_msgs::Marker::ADD;
    board_corner_points_marker.pose.orientation.w = 1.0;
    board_corner_points_marker.scale.x = 0.02;
    board_corner_points_marker.scale.y = 0.02;
    board_corner_points_marker.color.a = 1.0;
    board_corner_points_marker.color.b = 1.0;
    board_corner_points_marker.color.g = 1.0;
    board_corner_points_marker.color.r = 1.0;

    visualization_msgs::Marker board_edge_line_marker;
    board_edge_line_marker.header.frame_id = pcd_msg_in->header.frame_id;
    board_edge_line_marker.header.stamp = ros::Time();
    board_edge_line_marker.ns = "my_sphere";
    board_edge_line_marker.id = 10;
    board_edge_line_marker.type = visualization_msgs::Marker::LINE_STRIP;
    board_edge_line_marker.action = visualization_msgs::Marker::ADD;
    board_edge_line_marker.pose.orientation.w = 1.0;
    board_edge_line_marker.scale.x = 0.009;
    board_edge_line_marker.color.a = 1.0;
    board_edge_line_marker.color.b = 1.0;

    visualization_msgs::Marker corners_board;
    corners_board.header.frame_id = pcd_msg_in->header.frame_id;
    corners_board.header.stamp = ros::Time();
    corners_board.ns = "my_sphere";
    corners_board.type = visualization_msgs::Marker::SPHERE;
    corners_board.action = visualization_msgs::Marker::ADD;
    corners_board.pose.orientation.w = 1.0;
    corners_board.scale.x = 0.04;
    corners_board.scale.y = 0.04;
    corners_board.scale.z = 0.04;
    corners_board.color.a = 1.0;

    for (int i = 0; i < 5; i++)
    {
      if (i < 4)
      {
        // intersecting points - four corners of the board
        corners_board.pose.position.x = points_intersect->points[i].x;
        corners_board.pose.position.y = points_intersect->points[i].y;
        corners_board.pose.position.z = points_intersect->points[i].z;
      }
      else
      {
        // WHITE
        corners_board.pose.position.x = sample_data.lidar_point[0];
        corners_board.pose.position.y = sample_data.lidar_point[1];
        corners_board.pose.position.z = sample_data.lidar_point[2];
      }
      corners_board.id = i;
      switch (corners_board.id)
      {
        case 0:  // BLUE
          corners_board.color.r = 0.0;
          corners_board.color.g = 0.0;
          corners_board.color.b = 1.0;
          break;
        case 1:  // GREEN
          corners_board.color.r = 0.0;
          corners_board.color.g = 1.0;
          corners_board.color.b = 0.0;
          break;
        case 2:  // RED
          corners_board.color.r = 1.0;
          corners_board.color.g = 0.0;
          corners_board.color.b = 0.0;
          break;
        case 3:  // YELLOW
          corners_board.color.r = 1.0;
          corners_board.color.g = 1.0;
          corners_board.color.b = 0.0;
          break;
        default:  // WHITE
          corners_board.color.r = 1.0;
          corners_board.color.g = 1.0;
          corners_board.color.b = 1.0;
      }
      cb_pub.publish(corners_board);
    }

    // Visualize minimum and maximum points
    visualization_msgs::Marker minmax_pts;
    minmax_pts.header.frame_id = pcd_msg_in->header.frame_id;
    minmax_pts.header.stamp = ros::Time();
    minmax_pts.ns = "my_sphere";
    minmax_pts.type = visualization_msgs::Marker::SPHERE;
    minmax_pts.action = visualization_msgs::Marker::ADD;
    minmax_pts.pose.orientation.w = 1.0;
    minmax_pts.scale.x = 0.02;
    minmax_pts.scale.y = 0.02;
    minmax_pts.scale.z = 0.02;
    minmax_pts.color.a = 1.0;

    size_t y_min_pts = 0;
    for (y_min_pts = 0; y_min_pts < left_edge_points->points.size(); y_min_pts++)
    {
      minmax_pts.id = y_min_pts + 13;
      minmax_pts.pose.position.x = left_edge_points->points[y_min_pts].x;
      minmax_pts.pose.position.y = left_edge_points->points[y_min_pts].y;
      minmax_pts.pose.position.z = left_edge_points->points[y_min_pts].z;
      minmax_pts.color.b = 1.0;
      minmax_pts.color.r = 1.0;
      minmax_pts.color.g = 0.0;
      cb_pub.publish(minmax_pts);
    }
    for (size_t y_max_pts = 0; y_max_pts < right_edge_points->points.size(); y_max_pts++)
    {
      minmax_pts.id = y_min_pts + 13 + y_max_pts;
      minmax_pts.pose.position.x = right_edge_points->points[y_max_pts].x;
      minmax_pts.pose.position.y = right_edge_points->points[y_max_pts].y;
      minmax_pts.pose.position.z = right_edge_points->points[y_max_pts].z;
      minmax_pts.color.r = 0.0;
      minmax_pts.color.g = 1.0;
      minmax_pts.color.b = 1.0;
      cb_pub.publish(minmax_pts);
    }
    // Draw board edge lines
    for (int i = 0; i <= 2; i++)
    {
      geometry_msgs::Point p;
      p.x = points_intersect->points[1 - i % 2].x;
      p.y = points_intersect->points[1 - i % 2].y;
      p.z = points_intersect->points[1 - i % 2].z;
      board_corner_points_marker.points.push_back(p);
      board_edge_line_marker.points.push_back(p);
      p.x = points_intersect->points[3 - i].x;
      p.y = points_intersect->points[3 - i].y;
      p.z = points_intersect->points[3 - i].z;
      board_corner_points_marker.points.push_back(p);
      board_edge_line_marker.points.push_back(p);
    }

    // Publish board edges
    cb_pub.publish(board_edge_line_marker);

    // Visualize board normal vector
    board_norm_vec_marker.header.frame_id = pcd_msg_in->header.frame_id;
    board_norm_vec_marker.header.stamp = ros::Time();
    board_norm_vec_marker.ns = "my_namespace";
    board_norm_vec_marker.id = 12;
    board_norm_vec_marker.type = visualization_msgs::Marker::ARROW;
    board_norm_vec_marker.action = visualization_msgs::Marker::ADD;
    board_norm_vec_marker.scale.x = 0.02;
    board_norm_vec_marker.scale.y = 0.04;
    board_norm_vec_marker.scale.z = 0.06;
    board_norm_vec_marker.color.a = 1.0;
    board_norm_vec_marker.color.r = 0.0;
    board_norm_vec_marker.color.g = 0.0;
    board_norm_vec_marker.color.b = 1.0;
    geometry_msgs::Point start, end;
    start.x = sample_data.lidar_point[0];
    start.y = sample_data.lidar_point[1];
    start.z = sample_data.lidar_point[2];
    end.x = start.x + sample_data.lidar_normal[0] / 2;
    end.y = start.y + sample_data.lidar_normal[1] / 2;
    end.z = start.z + sample_data.lidar_normal[2] / 2;
    board_norm_vec_marker.points.resize(2);
    board_norm_vec_marker.points[0].x = start.x;
    board_norm_vec_marker.points[0].y = start.y;
    board_norm_vec_marker.points[0].z = start.z;
    board_norm_vec_marker.points[1].x = end.x;
    board_norm_vec_marker.points[1].y = end.y;
    board_norm_vec_marker.points[1].z = end.z;
    vis_pub.publish(board_norm_vec_marker);

  }  // if (flag == 1)

  // Feature data is published (chosen) only if 'enter' is pressed
  if (flag == 4)
  {
    roi_publisher.publish(sample_data);
    flag = 0;
  }  // if (flag == 4)
}  // End of extractROI

void feature_extraction::add_example_set()
{
  // Working Example
  int sample_size = 11;
  double lidar_points_m[sample_size][3] = {
    5.88294792175293,    0.2601484954357147,   -0.516505241394043,   5.630581855773926,   0.6023352146148682,
    -0.4996507465839386, 5.578598976135254,    0.008569240570068359, -0.5114462375640869, 5.602836608886719,
    -0.4298157095909119, -0.5022851228713989,  5.049532890319824,    -0.5479730367660522, -0.5472280979156494,
    4.952737331390381,   -0.04899677634239197, -0.5072450041770935,  4.89508056640625,    0.3235059678554535,
    -0.5044921636581421, 4.934260368347168,    0.7984897494316101,   -0.4899445176124573, 4.828236103057861,
    0.9990785717964172,  -0.4861305356025696,  4.828236103057861,    0.9990785717964172,  -0.4861305356025696,
    4.525825500488281,   0.3605735301971436,   -0.522205650806427
  };
  double lidar_normals_m[sample_size][3] = {
    -0.9977201819419861,   0.06543730944395065,  0.01650358736515045,  -0.986467719078064,    -0.163927748799324,
    -0.003030248684808612, -0.9746381044387817,  0.2237807065248489,   -0.001669091172516346, -0.9730937480926514,
    0.2301121801137924,    0.01170309074223042,  -0.5918497443199158,  0.8059107661247253,    0.0148917268961668,
    -0.9780668020248413,   0.2080752402544022,   0.009487774223089218, -0.9409900903701782,   -0.3384327590465546,
    -0.000993498251773417, -0.9547579288482666,  -0.297264575958252,   -0.008424205705523491, -0.9005329012870789,
    -0.4347468912601471,   0.005973819643259048, -0.9005329012870789,  -0.4347468912601471,   0.005973819643259048,
    -0.9980796575546265,   0.06093011423945427,  0.0111602358520031
  };
  double lidar_corners_m[sample_size][3] = {
    .87644100189209,     0.3465303778648376,   -1.252355813980103, 5.631660461425781,   0.6092511415481567,
    -1.22482430934906,   5.584787845611572,    0.0300215482711792, -1.249149441719055,  5.593831062316895,
    -0.4294111132621765, -1.259073972702026,   5.07119083404541,   -0.5191781520843506, -1.244774103164673,
    4.938187122344971,   -0.07969903945922852, -1.333822965621948, 4.898911952972412,   0.3151298761367798,
    -1.279770612716675,  4.937174320220947,    0.8120604753494263, -1.299078226089478,  4.825445175170898,
    0.9940336942672729,  -1.274038791656494,   4.825445175170898,  0.9940336942672729,  -1.274038791656494,
    4.517427444458008,   0.3539302349090576,   -1.237077713012695
  };
  double camera_points_m[sample_size][3] = {
    0.02041523947176185,  -0.04304173613879077,  5.579907951663899,    -0.2864001231498804,  -0.05072438977431445,
    5.331202091787546,    0.2772612484039199,    -0.0188520947490928,  5.277599321147195,    0.7273957650247721,
    0.002975621847255206, 5.287756885388936,     0.7962654681850951,   0.02598115677759156,  4.756127390469947,
    0.3399282991497449,   0.004351537971606296,  4.660282106140917,    -0.02437555575011066, -0.01151935385924169,
    4.606839917237708,    -0.4811280642777177,   -0.03856684253478274, 4.652601587287926,    -0.6757410460346222,
    -0.04746345562227511, 4.560184599946739,     -0.6757410460346222,  -0.04746345562227511, 4.560184599946739,
    -0.07442677350852553, -0.005879277628024968, 4.250668868369975
  };
  double camera_normals_m[sample_size][3] = {
    -0.1762917236420798,  -0.07289023820965369, -0.981635493118013,   0.2494584063496895,   0.1070182793923948,
    -0.9624539424706849,  -0.2856914183275749,  -0.05524743224212879, -0.9567278268790084,  -0.3246014428663737,
    -0.07265805511731005, -0.9430560483426413,  -0.8228203088772332,  -0.04059490890429954, -0.5668498854813556,
    -0.2817149729733174,  -0.06183692610337618, -0.957503456167511,   0.3592396331925115,   0.09400254054047055,
    -0.9284990082469916,  0.3434636959198194,   0.1067226118679418,   -0.9330825117325812,  0.4653568770988404,
    0.1035155205680405,   -0.879049210191524,   0.4653568770988404,   0.1035155205680405,   -0.879049210191524,
    -0.162868728613596,   -0.065833907293708,   -0.984448918883166
  };
  double pixel_data_m[sample_size] = { 0.08816360111310213, 0.09215283191975748, 0.09322790742890379,
                                       0.09269628569137914, 0.1020910397712238,  0.1055091784685223,
                                       0.1068452882463528,  0.1054687977369695,  0.107429852046491,
                                       0.107429852046491,   0.1157655074279804 };

  for (int row = 0; row < sample_size; row++)
  {
    for (int col = 0; col < 3; col++)
    {
      sample_data.lidar_point[col] = lidar_points_m[row][col];
      sample_data.lidar_normal[col] = lidar_normals_m[row][col];
      sample_data.camera_point[col] = camera_points_m[row][col];
      sample_data.camera_normal[col] = camera_normals_m[row][col];
      sample_data.lidar_corner[col] = lidar_corners_m[row][col];
    }
    sample_data.pixel_data = pixel_data_m[row];
    roi_publisher.publish(sample_data);
    ros::Duration(0.1).sleep();
  }
}

}  // namespace extrinsic_calibration
